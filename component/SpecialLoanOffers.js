import React, { useState } from "react";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";

const SpecialLoanOffers = ({ data }) => {
    const [modalContent, setModalContent] = useState(null);
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    return (
        <>
            {show ? (
                <>
                    <Modal show={show} onHide={handleClose} size="lg">
                        <Modal.Header closeButton>
                            <h5 className="text-success">Disclaimer</h5>
                        </Modal.Header>
                        <Modal.Body className="small">
                            {modalContent}
                        </Modal.Body>
                    </Modal>
                </>
            ) : null}

            <section
                className="offer border"
            >
                <div className=" bg-white">
                    <div className="row text-dark ">
                        <div className="col col-md-12 mx-3 mb-3 pt-2 ">
                            <div className="field-group">
                                <p className="sub-title ">
                                    <span
                                        style={{ color: "#ff7200 " }}
                                    >
                                        {" "}
                                        Congratulations!{" "}
                                    </span>{" "}
                                    We have matched you with the following offers.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            {data.specialOffers.map((curOffer, i) => {
                return (
                    <section
                        key={i}
                        className="offer pt-3 mt-5 mb-5 shadow-lg rounded"
                    >
                        <div className=" bg-white">
                            <div className="row five-col-row">
                                <div className="col col-md-2 mx-3  text-white fs-6">
                                    <p className="ribbon">
                                        <span className="text">
                                            <strong>
                                                {" "}
                                                <svg
                                                    xmlns="http://www.w3.org/2000/svg"
                                                    width="16"
                                                    height="16"
                                                    fill="currentColor"
                                                    className="bi bi-check-circle"
                                                    viewBox="0 0 16 16"
                                                >
                                                    <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                                                    <path d="M10.97 4.97a.235.235 0 0 0-.02.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-1.071-1.05z" />
                                                </svg>
                                            </strong>{" "}
                                            SPECIAL OFFER"
                                                
                                        </span>
                                    </p>
                                </div>
                                {/* <div className="col col-md-2 mx-3 text-dark ">
                                    <p className="fs-6">
                                        <span className="text-capitalize">
                                            {curOffer.productSubType.replace(
                                                "_",
                                                " "
                                            )}
                                        </span>{" "}
                                        -{" "}
                                        {curOffer.secured
                                            ? "Secured"
                                            : "Unsecured"}
                                    </p>
                                </div> */}

                                <div className="col-xl-3 col-lg-6 col-md-12 col-sm-12 text-dark ">
                                    <h6 className="display-5"></h6>
                                </div>
                                <div className="col-xl-3 col-lg-6 col-md-12 col-sm-12 text-dark ">
                                    <h6 className="display-5"></h6>
                                </div>
                                <div className="col-xl-3 col-lg-6 col-md-12 col-sm-12 text-dark ">
                                    <h6 className="display-5"></h6>
                                </div>
                            </div>
                            <div className="row  mb-3 text-dark ">
                                <div className="col col-md-2 mx-3 mb-4 ">
                                    <div className="field-group">
                                        <a
                                            className="navbar-brand"
                                            href="https://radcred.com/"
                                        >
                                            <img
                                                src={
                                                    curOffer
                                                        .partnerImageUrl
                                                }
                                                className="main-logo"
                                                alt={
                                                    curOffer
                                                        .partnerName
                                                }
                                            />
                                        </a>

                                        <div className="icon">
                                            {/* <a
                                                role="button"
                                                onClick={() => {
                                                    setShow(true);
                                                    setModalContent(
                                                        curOffer
                                                            .originator
                                                            .disclaimer
                                                    );
                                                }}
                                                className=" text-muted fs-6"
                                            >

                                            </a> */}
                                            <div className="icon">
                                                <h5 href="#" className="lender-title">{curOffer.partnerName}</h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col col-md-6 mx-8 mb-12 ">
                                    <div className="field-group">
                                        <span className="lender-title">
                                            {curOffer.name}
                                        </span>
                                        <div className="icon p-b-25">
                                            <p className="special-offer-text text-muted" dangerouslySetInnerHTML={{__html: curOffer.desc}}></p>
                                        </div>
                                    </div>
                                </div>


                                <div className="col col-md-2 text-center">
                                    <div className="field-group">
                                        <div className="form-note">
                                            <div className="field-group m-t-35">
                                                <a
                                                    href={
                                                        curOffer.url
                                                    }
                                                    target="_blank"
                                                >
                                                <Button variant="success">CONTINUE</Button>
                                                </a>
                                                
                                                <div className="field-group mt-3"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                );
            })}
        </>
    );
};

export default SpecialLoanOffers;
