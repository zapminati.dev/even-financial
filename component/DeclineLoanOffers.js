import React, { useState } from "react";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";

const DeclineLoanOffers = () => {

    return (
        <>

            <section
                className="offer border"
            >
                <div className=" bg-white">
                    <div className="row text-dark ">
                        <div className="col col-md-12 mx-3 mb-3 pt-2 ">
                            <div className="field-group">
                                <h5 className="sub-title text-center">
                                    <span
                                        style={{ color: "#FF0000 " }}
                                    >
                                        {" "}
                                        Oops!{" "}
                                    </span>{" "}
                                </h5>
                                <p>
                                    It looks like we cannot match you with loan opportunities at this time. However, we’ve matched you with the following offers to fit individual financial needs.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>


        </>
    );
};

export default DeclineLoanOffers;
