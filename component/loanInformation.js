import React, { useState } from "react";
import Link from "next/link";
import validator from "validator";

const StepTwo = ({ nextStep, handleFormData, prevStep, values }) => {
     const [error, setError] = useState(false);

     // after form submit validating the form data using validator
     const submitFormData = (e) => {
          e.preventDefault();

          // checking if value of first name and last name is empty show error else take to step 2
          if (
               //validator.isEmpty(toString(values.loanPurpose)) ||
               //validator.isEmpty(toString(values.loanAmount))
               values.loanPurpose === undefined || values.loanPurpose === ""
                    ? true
                    : "" || validator.isLength(values.loanPurpose)
                    ? ""
                    : true ||
                      values.loanAmount === undefined ||
                      values.loanAmount === ""
                    ? true
                    : "" ||
                      validator.isLength(values.loanAmount, { min: 5, max: 5 })
                    ? ""
                    : true
          ) {
               setError(true);
          } else {
               setError(false);
               nextStep();
          }
     };

     return (
          <>
               <section className="sign-in-section p-t-120 p-b-120">
                    <div className="container">
                         <div className="row justify-content-center">
                              <div className="col-xl-5 col-lg-6 col-md-8 col-sm-10">
                                   <div className="mb-3">
                                        <div className="progress">
                                             <div
                                                  className="progress-bar"
                                                  role="progressbar"
                                                  style={{ width: "28%" }}
                                                  aria-valuenow="28"
                                                  aria-valuemin="0"
                                                  aria-valuemax="100"
                                             ></div>
                                        </div>
                                   </div>
                                   <div className="sign-in-up-wrapper">
                                        <form onSubmit={submitFormData}>
                                             <div className="form-groups">
                                                  <h4 className="form-title">
                                                       Loan Purpose
                                                  </h4>
                                                  <div className="field-group">
                                                       <div className="icon">
                                                            <i className="fas fa-chart-line"></i>
                                                       </div>

                                                       <select
                                                            name="loanPurpose"
                                                            defaultValue={
                                                                 values.loanPurpose
                                                            }
                                                            onChange={handleFormData(
                                                                 "loanPurpose"
                                                            )}
                                                            className={
                                                                 error
                                                                      ? "border border-danger"
                                                                      : ""
                                                            }
                                                            required
                                                            oninvalid="this.setCustomValidity('Enter User Name Here')"
                                                            oninput="this.setCustomValidity('')"
                                                       >
                                                            <option value="">
                                                                 Select Loan
                                                                 Purpose
                                                            </option>
                                                            <option value="debt_consolidation">
                                                                 Debt
                                                                 Consolidation
                                                            </option>
                                                            <option value="home_improvement">
                                                                 Home
                                                                 Improvement
                                                            </option>
                                                            <option value="student_loan">
                                                                 Student Loan
                                                            </option>
                                                            <option value="wedding">
                                                                 Wedding
                                                            </option>
                                                            <option value="large_purchases">
                                                                 {" "}
                                                                 Large Purchases
                                                            </option>
                                                            <option value="auto">
                                                                 Auto
                                                            </option>
                                                            <option value="baby">
                                                                 Baby
                                                            </option>
                                                            <option value="boat">
                                                                 Boat
                                                            </option>
                                                            <option value="business">
                                                                 Business
                                                            </option>
                                                            <option value="household_expenses">
                                                                 Household
                                                                 Expenses
                                                            </option>
                                                            <option value="medical_dental">
                                                                 Medical Dental
                                                            </option>
                                                            <option value="moving_relocation">
                                                                 Moving
                                                                 Relocation
                                                            </option>
                                                            <option value="taxes">
                                                                 Taxes
                                                            </option>
                                                            <option value="vacation">
                                                                 Vacation
                                                            </option>
                                                            <option value="other">
                                                                 Other
                                                            </option>
                                                       </select>
                                                  </div>
                                                  <div className="field-group">
                                                       <div className="icon">
                                                            <svg
                                                                 xmlns="http://www.w3.org/2000/svg"
                                                                 width="16"
                                                                 height="16"
                                                                 fill="currentColor"
                                                                 className="bi bi-bank"
                                                                 viewBox="0 0 16 16"
                                                            >
                                                                 <path d="m8 0 6.61 3h.89a.5.5 0 0 1 .5.5v2a.5.5 0 0 1-.5.5H15v7a.5.5 0 0 1 .485.38l.5 2a.498.498 0 0 1-.485.62H.5a.498.498 0 0 1-.485-.62l.5-2A.501.501 0 0 1 1 13V6H.5a.5.5 0 0 1-.5-.5v-2A.5.5 0 0 1 .5 3h.89L8 0ZM3.777 3h8.447L8 1 3.777 3ZM2 6v7h1V6H2Zm2 0v7h2.5V6H4Zm3.5 0v7h1V6h-1Zm2 0v7H12V6H9.5ZM13 6v7h1V6h-1Zm2-1V4H1v1h14Zm-.39 9H1.39l-.25 1h13.72l-.25-1Z" />
                                                            </svg>
                                                       </div>
                                                       <input
                                                            name="loanAmount"
                                                            defaultValue={
                                                                 values.loanAmount
                                                            }
                                                            type="number"
                                                            placeholder="Loan Amount"
                                                            onChange={handleFormData(
                                                                 "loanAmount"
                                                            )}
                                                            className={
                                                                 error
                                                                      ? "border border-danger"
                                                                      : ""
                                                            }
                                                            required
                                                       />
                                                  </div>
                                             </div>
                                             <div className="form-note">
                                                  <div className="field-group">
                                                       <button type="submit">
                                                            Next
                                                       </button>
                                                  </div>
                                             </div>
                                        </form>
                                   </div>
                              </div>
                         </div>
                    </div>
               </section>
          </>
     );
};

export default StepTwo;
