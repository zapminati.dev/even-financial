import React, { useState } from "react";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";

const LoanOffersList = ({ data }) => {
     const [modalContent, setModalContent] = useState(null);
     const [show, setShow] = useState(false);

     const handleClose = () => setShow(false);
     const handleShow = () => setShow(true);

     return (
          <>
               {show ? (
                    <>
                         <Modal show={show} onHide={handleClose} size="lg">
                              <Modal.Header closeButton>
                                   <h5 className="text-success">Disclaimer</h5>
                              </Modal.Header>
                              <Modal.Body className="small">
                                   {modalContent}
                              </Modal.Body>
                         </Modal>
                    </>
               ) : null}

               <section
                    className="offer border"
               >
                    <div className=" bg-white">
                         <div className="row text-dark ">
                              <div className="col col-md-12 mx-3 mb-3 pt-2 ">
                                   <div className="field-group">
                                        <p className="sub-title ">
                                             <span
                                                  style={{ color: "#ff7200 " }}
                                             >
                                                  {" "}
                                                  Congratulations!{" "}
                                             </span>{" "}
                                             We have matched you with these top
                                             offers to consolidate your debt.
                                        </p>
                                   </div>
                              </div>
                         </div>
                    </div>
               </section>

               {data.loanOffers.map((curOffer, i) => {
                    return (
                         <section
                              key={i}
                              className="offer pt-3 mt-5 mb-5 shadow-lg rounded"
                         >
                              <div className=" bg-white">
                                   <div className="row five-col-row">
                                        <div className="col col-md-2 mx-3  text-white fs-6">
                                             <p className="ribbon">
                                                  <span className="text">
                                                       <strong>
                                                            {" "}
                                                            <svg
                                                                 xmlns="http://www.w3.org/2000/svg"
                                                                 width="16"
                                                                 height="16"
                                                                 fill="currentColor"
                                                                 className="bi bi-check-circle"
                                                                 viewBox="0 0 16 16"
                                                            >
                                                                 <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                                                                 <path d="M10.97 4.97a.235.235 0 0 0-.02.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-1.071-1.05z" />
                                                            </svg>
                                                       </strong>{" "}
                                                       {curOffer.preApproved
                                                            ? "PRE APPROVED"
                                                            : ""}
                                                       {curOffer.preQualified
                                                            ? "PRE QUALIFIED"
                                                            : ""}
                                                  </span>
                                             </p>
                                        </div>
                                        <div className="col col-md-2 mx-3 text-dark ">
                                             <p className="fs-6">
                                                  <span className="text-capitalize">
                                                       {curOffer.productSubType.replace(
                                                            "_",
                                                            " "
                                                       )}
                                                  </span>{" "}
                                                  -{" "}
                                                  {curOffer.secured
                                                       ? "Secured"
                                                       : "Unsecured"}
                                             </p>
                                        </div>

                                        <div className="col-xl-3 col-lg-6 col-md-12 col-sm-12 text-dark ">
                                             <h6 className="display-5"></h6>
                                        </div>
                                        <div className="col-xl-3 col-lg-6 col-md-12 col-sm-12 text-dark ">
                                             <h6 className="display-5"></h6>
                                        </div>
                                        <div className="col-xl-3 col-lg-6 col-md-12 col-sm-12 text-dark ">
                                             <h6 className="display-5"></h6>
                                        </div>
                                   </div>
                                   <div className="row  mb-3 text-dark ">
                                        <div className="col col-md-2 mx-3 mb-4 ">
                                             <div className="field-group">
                                                  <a
                                                       className="navbar-brand"
                                                       href="https://radcred.com/"
                                                  >
                                                       <img
                                                            src={
                                                                 curOffer
                                                                      .originator
                                                                      .images[0]
                                                                      .url
                                                            }
                                                            className="main-logo"
                                                            alt={
                                                                 curOffer
                                                                      .originator
                                                                      .name
                                                            }
                                                       />
                                                  </a>

                                                  <div className="icon">
                                                       <a
                                                            role="button"
                                                            onClick={() => {
                                                                 setShow(true);
                                                                 setModalContent(
                                                                      curOffer
                                                                           .originator
                                                                           .disclaimer
                                                                 );
                                                            }}
                                                            className=" text-muted fs-6"
                                                       >
                                                            <u>
                                                                 Disclaimer &
                                                                 more Info
                                                            </u>
                                                       </a>
                                                  </div>
                                             </div>
                                        </div>
                                        <div className="col col-md-2 mx-3 mb-4 ">
                                             <div className="field-group">
                                                  <span className="form-title">
                                                       {curOffer.termLength}{" "}
                                                       months
                                                  </span>
                                                  <div className="icon">
                                                       <p className="text-muted fs-6">
                                                            Terms of Loans
                                                       </p>
                                                  </div>
                                             </div>
                                        </div>
                                        <div className="col col-md-2 mx-3 mb-4 ">
                                             <div className="field-group">
                                                  <span className="form-title">
                                                       {curOffer.maxApr}%
                                                  </span>
                                                  <div className="icon">
                                                       <p className="text-muted fs-6">
                                                            <span className="text-capitalize">
                                                                 {
                                                                      curOffer.aprType
                                                                 }
                                                            </span>{" "}
                                                            APR
                                                       </p>
                                                  </div>
                                             </div>
                                        </div>
                                        <div className="col col-md-2 mx-3 mb-4 ">
                                             <div className="field-group">
                                                  <span className="form-title">
                                                       $
                                                       {
                                                            curOffer.maxMonthlyPayment
                                                       }
                                                  </span>
                                                  <div className="icon">
                                                       <p className="text-muted fs-6">
                                                            Est. Monthly Payment
                                                       </p>
                                                  </div>
                                             </div>
                                        </div>

                                        <div className="col col-md-2 mx-3 mb-4 text-center">
                                             <div className="field-group">
                                                  <h5 className="form-title">
                                                       ${curOffer.maxAmount}
                                                  </h5>
                                                  <div className="form-note">
                                                       <div className="field-group ">
                                                            <a
                                                                 href={
                                                                      curOffer.url
                                                                 }
                                                                 target="_blank"
                                                                 className="btn bg-success offer-button text-white"
                                                            >
                                                                 CONTINUE
                                                            </a>
                                                            <div className="field-group mt-3"></div>
                                                       </div>
                                                  </div>
                                                  <div className="icon">
                                                       <a
                                                            href={curOffer.url}
                                                            className="text-muted text-decoration-underline fs-6"
                                                       >
                                                            <u>
                                                                 See all Offers
                                                                 through{" "}
                                                                 {
                                                                      curOffer
                                                                           .originator
                                                                           .name
                                                                 }
                                                            </u>
                                                       </a>
                                                  </div>
                                             </div>
                                        </div>
                                   </div>
                              </div>
                         </section>
                    );
               })}
          </>
     );
};

export default LoanOffersList;
