import React, { useState } from "react";
import Link from "next/link";
import validator from "validator";
import PreLoader from "./layouts/PreLoader";
import { useRouter } from "next/router";
import moment from "moment";

const StepSeven = ({ nextStep, handleFormData, prevStep, values }) => {
     const router = useRouter();

     const [error, setError] = useState(false);
     const [message, setMessage] = useState("");
     const [Loading, setLoading] = useState(false);

     //destructuring the object from values
     const {
          firstName,
          lastName,
          mobile,
          city,
          email,
          address,
          address2,
          state,
          zip,
          dateOfBirth,
          socialSecurityNumber,
          loanPurpose,
          loanAmount,
          propertyStatus,
          providedCreditRating,
          employmentStatus,
          payFrequency,
          annualIncome,
          educationLevel,
     } = values;

     let handleSubmit = async (e) => {
          e.preventDefault();
          setLoading(true);

          const token = "11a972ae-da8a-4898-87b5-dad4e2af7744_a0bfd6ef-1983-469e-bae3-afac3ea4330c"; //Production
          //const token = "686f05a9-1a0b-442c-a48c-fd73ad6a748f_53679f03-70ac-4808-96a6-b387e022d916"; //Testing

          try {
               let res = await fetch(
                    "https://api.evenfinancial.com/leads/rateTables",
                    {
                         method: "POST",
                         headers: {
                              "Content-type": "application/json",
                              "Authorization": `Bearer ${token}`,
                         },
                         body: JSON.stringify({
                              productTypes: ["other", "loan"],
                              personalInformation: {
                                   firstName: firstName,
                                   lastName: lastName,
                                   email: email,
                                   city: city,
                                   state: state,
                                   primaryPhone: mobile,
                                   address1: address,
                                   address2: address2,
                                   zipcode: zip,
                                   dateOfBirth: moment(dateOfBirth).format("YYYY-MM-DD"),
                                   ssn: socialSecurityNumber,
                              },
                              loanInformation: {
                                   purpose: loanPurpose,
                                   loanAmount: parseInt(loanAmount),
                              },
                              mortgageInformation: {
                                   propertyStatus: propertyStatus,
                              },
                              creditInformation: {
                                   providedCreditRating: providedCreditRating,
                              },
                              financialInformation: {
                                   employmentStatus: employmentStatus,
                                   employmentPayFrequency: payFrequency,
                                   annualIncome: parseInt(annualIncome),
                              },
                              educationInformation: {
                                   educationLevel: educationLevel,
                              },
                              legalInformation: {
                                   consentsToFcra: true,
                                   consentsToTcpa: true,
                              },  
                         }),
                    }
               );
               let resJson = await res.json();
               
               if (res.status === 200) {
                    //router.push(`/loan-offers/${resJson.uuid}`);
                    
                    setMessage("Data Fetched Successfully!");
                    const reqRes = await fetch("/api/loanRequest/", {
                         method: "POST",
                         body: JSON.stringify({
                              uuid: resJson.uuid,
                              leadUuid: resJson.leadUuid,
                         }),
                    });

                    const res = await reqRes.json();

                    if (reqRes.status === 200) {
                         router.push(`/loan-offers/${res.loanRequest.uuid}`);
                    }
               } else if(res.status === 400){
                    setMessage(resJson.message);
               } else {
                    setMessage("Oops! something wrong, please check form again.");
               }
          } catch (err) {
               console.log(err);
          }
     };

     // after form submit validating the form data using validator
     //  const submitFormData = (e) => {
     //       e.preventDefault();

     //       // checking if value of first name and last name is empty show error else take to step 2
     //       if (validator.isEmpty(toString(values.primaryPhone))) {
     //            setError(true);
     //       } else {
     //            setError(false);
     //            nextStep();
     //       }
     //  };

     return (
          <>
               {Loading ? (
                    <PreLoader />
               ) : (
                    <section className="sign-in-section p-t-120 p-b-120">
                         <div className="container">
                              <div className="row justify-content-center">
                                   <div className="col-12">
                                        <h5 className="text-danger">{message}</h5>
                                   </div>
                                   <div className="col-xl-5 col-lg-6 col-md-8 col-sm-10">
                                        <div className="mb-3">
                                             <div className="progress">
                                                  <div
                                                       className="progress-bar"
                                                       role="progressbar"
                                                       style={{ width: "100%" }}
                                                       aria-valuenow="100"
                                                       aria-valuemin="0"
                                                       aria-valuemax="100"
                                                  ></div>
                                             </div>
                                        </div>
                                        <div className="sign-in-up-wrapper">
                                             <form onSubmit={handleSubmit}>
                                                  <div className="form-groups">
                                                       <h4 className="form-title">
                                                            Legal Information
                                                       </h4>

                                                       {/* <div className="field-group text-decoration-revert">
                                                            <a href="#">
                                                                 Affirmative
                                                                 Consent to FCRA{" "}
                                                            </a>
                                                       </div>

                                                       <div className="field-group text-decoration-revert">
                                                            <a href="#">
                                                                 Affirmative
                                                                 Consent to TCPA{" "}
                                                            </a>
                                                       </div>

                                                       <div className="field-group form-check">
                                                            <input
                                                                 type="checkbox"
                                                                 className=""
                                                                 id="checkboxNoLabel"
                                                                 value=""
                                                                 aria-label="..."
                                                                 required
                                                            />
                                                       </div> */}
                                                  </div>

                                                  <div className="form-note">
                                                       <div className="field-group">
                                                            <button type="submit">
                                                                 Agree and
                                                                 Countinue
                                                            </button>
                                                            <div className="field-group mt-3">
                                                                 <Link href="#">
                                                                      <a
                                                                           type="submit"
                                                                           onClick={
                                                                                prevStep
                                                                           }
                                                                      >
                                                                           Back
                                                                      </a>
                                                                 </Link>
                                                            </div>
                                                            <p className="">
                                                            By clicking Agree and Continue, I hereby consent to the <a href="https://radcred.com/advertiser-disclosure/#E-sign" target="_blank">"E-Sign Agreement"</a>, the <a href="https://radcred.com/advertiser-disclosure/#credit-authorization" target="_blank">"Credit Authorization
                                                                 Agreement"</a>, the <a href="https://radcred.com/terms-condition/" target="_blank">Terms of Service</a> and <a href="https://radcred.com/privacy-policy/" target="_blank">Privacy Policy</a>, and 
                                                                 I am providing written consent under the Fair Credit Reporting Act (FCRA) for (Even Financial), 
                                                                 and its partners and financial institutions to obtain consumer report information from my credit profile. I agree to be contacted by Radcred
                                                            and its partners and their affiliated companies via email and/or at the telephone number(s) I have provided
                                                            above to explore various financial products and services I inquired about, including contact through automatic
                                                            dialing systems, systems, artificial or pre-recorded voice messaging, or text message. Consent is not required as
                                                            a condition to utilize Radcred, and you may choose to be contacted by an individual customer care representative(s). The Financial Institutions will be performing the soft pull on applicants.
                                                            </p>
                                                       </div>
                                                  </div>
                                             </form>
                                        </div>
                                   </div>
                              </div>
                         </div>
                    </section>
               )}
          </>
     );
};

export default StepSeven;
