import React, { useState } from "react";
import Link from "next/link";
import validator from "validator";

const StepSix = ({ nextStep, handleFormData, prevStep, values }) => {
    const [error, setError] = useState(false);

    // after form submit validating the form data using validator
    const submitFormData = (e) => {
        e.preventDefault();


        // checking if value of first name and last name is empty show error else take to step 2
        if (
            //validator.isEmpty(toString(values.educationLevel))
            values.educationLevel === undefined || values.educationLevel === "" ? true : "" ||
                validator.isLength(values.educationLevel) ? "" : true
        ) {
            setError(true);
        } else {
            setError(false);
            nextStep();
        }
    };



    return (
        <>
            <section className="sign-in-section p-t-120 p-b-120">
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-xl-5 col-lg-6 col-md-8 col-sm-10">
                            <div className="mb-3">
                                <div className="progress">
                                    <div
                                        className="progress-bar"
                                        role="progressbar"
                                        style={{ width: "85%" }}
                                        aria-valuenow="85"
                                        aria-valuemin="0"
                                        aria-valuemax="85"
                                    ></div>
                                </div>
                            </div>
                            <div className="sign-in-up-wrapper">
                                <form onSubmit={submitFormData}>
                                    <div className="form-groups">
                                        <h4 className="form-title">Education Information</h4>
                                        <div className="field-group">
                                            <div className="icon">
                                                <i className="	fas fa-user-graduate"></i>
                                            </div>

                                            <select name="educationLevel"
                                                defaultValue={values.educationLevel}
                                                onChange={handleFormData("educationLevel")}
                                                className={error ? "border border-danger" : ""}
                                                required
                                                >
                                                <option value="">Select Education Level</option>
                                                <option value="high_school">High School</option>
                                                <option value="associate">Associate</option>
                                                <option value="Bachelors">Bachelors</option>
                                                <option value="masters">Masters</option>
                                                <option value="Doctorate">Doctorate</option>
                                                <option value="other_grad_degree">Other Grad Degree</option>
                                                <option value="certificate">Certificate</option>
                                                <option value="did_not_graduate">Did Not Graduate</option>
                                                <option value="Still Enrolled">still_enrolled</option>
                                                <option value="other">Other</option>
                                            </select>

                                        </div>


                                    </div>
                                    <div className="form-note">
                                        <div className="field-group">
                                            <button type="submit">Next</button>
                                        </div>
                                        <div className="field-group">
                                            <Link href="#">
                                                <a type="submit" onClick={prevStep}>
                                                    Back
                                                </a>
                                            </Link>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    );
};

export default StepSix;
