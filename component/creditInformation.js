import React, { useState } from "react";
import Link from "next/link";
import validator from "validator";

const StepFour = ({ nextStep, handleFormData, prevStep, values }) => {
    const [error, setError] = useState(false);

    // after form submit validating the form data using validator
    const submitFormData = (e) => {
        e.preventDefault();

        // checking if value of first name and last name is empty show error else take to step 2
        if (
            //validator.isEmpty(toString(values.providedCreditRating))
            values.providedCreditRating === undefined || values.providedCreditRating === "" ? true : "" ||
                validator.isLength(values.providedCreditRating) ? "" : true

        ) {
            setError(true);
        } else {
            setError(false);
            nextStep();
        }
    };

    return (
        <>
            <section className="sign-in-section p-t-120 p-b-120">
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-xl-5 col-lg-6 col-md-8 col-sm-10">
                            <div className="mb-3">
                                <div className="progress">
                                    <div
                                        className="progress-bar"
                                        role="progressbar"
                                        style={{ width: "56%" }}
                                        aria-valuenow="56"
                                        aria-valuemin="0"
                                        aria-valuemax="56"
                                    ></div>
                                </div>
                            </div>
                            <div className="sign-in-up-wrapper">
                                <form onSubmit={submitFormData}>
                                    <div className="form-groups">
                                        <h4 className="form-title">Credit Information</h4>
                                        <div className="field-group">
                                            <div className="icon">
                                                <i className="fas fa-star"></i>
                                            </div>


                                            <select name="providedCreditRating"
                                                defaultValue={values.providedCreditRating}
                                                onChange={handleFormData("providedCreditRating")}
                                                className={error ? "border border-danger" : ""}
                                                required
                                                >
                                                <option value="">Select Credit Rating</option>
                                                <option value="excellent">Excellent (720+) </option>
                                                <option value="good">Good (720+) </option>
                                                <option value="fair">Fair (620-659)</option>
                                                <option value="poor">Poor (620-659)</option>
                                                <option value="Limited">Limited/No History</option>
                                            </select>

                                        </div>
                                    </div>
                                    <div className="form-note">
                                        <div className="field-group">
                                            <button type="submit">Next</button>
                                        </div>
                                        <div className="field-group">
                                            <Link href="#">
                                                <a type="submit" onClick={prevStep}>
                                                    Back
                                                </a>
                                            </Link>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    );
};

export default StepFour;
