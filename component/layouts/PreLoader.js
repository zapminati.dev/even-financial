const PreLoader = () => {
  return (
    <div id="preloader">
      <img
        className="preloader-image"
        width="60"
        src="/assets/img/loading-forever.gif"
        alt="preloader"
      />
    </div>
  );
};

export default PreLoader;
