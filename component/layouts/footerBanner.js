import React from 'react';

const FooterBanner = () => {
    return (
        <>


            <section className="newsletter-area bg-cover-center bg-soft-white-color p-t-20 p-b-20" >


                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-lg-7">
                            <div className="newsletter-text">
                                <div className="common-heading tagline-boxed text-center m-b-40 p-0 px-5">
                                    <img className="particle-1 animate-float-bob-y" src="/assets/img/icon/secure.png" alt="particle Two" />
                                    <h3 className="sub-title ">Safe and Secure</h3>
                                    <p>We use 256-bit encryption to secure your information — a higher security standard than many banks.</p>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </section>

        </>
    )
}
export default FooterBanner;

