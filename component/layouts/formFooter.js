import React from 'react';


const FormFooter = () => {
    return (
        <>
            <section className="sign-in-section p-t-50 p-b-50">
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-xl-6 col-lg-8 col-md-12 col-sm-12">
                            <div className="copyright-area">

                                <div className="col-md-12">
                                    <ul className="footer-nav  m-b-sm-15 bottom-footer-form d-flex flex-row footer-form-all justify-content-center ">
                                        <li><a href="https://radcred.com/privacy-policy/" target='_blank'> Policy</a></li>
                                        <li><a href="https://radcred.com/terms-of-website-use/" target="_blank">Terms</a></li>
                                        <li><a href="https://radcred.com/contact/" target="_blank">Support</a></li>
                                        <li><a href="https://radcred.com/ad-disclosure/" target="_blank">Disclosures</a></li>

                                        <li>Powered by <a href="https://radcred.com/about-us/" target="_blank">radcred</a></li>
                                    </ul>
                                </div>



                            </div>
                        </div>
                    </div>
                </div>
            </section>



        </>
    )
}
export default FormFooter;

