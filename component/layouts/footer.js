import React from 'react';

const Footer = () => {
    return (

        <section className="template-footer webinar-footer" >
            <div className="footer-newsletters">

                <div className="copyright-area bg-light px-5">
                    <div className="row px-2">
                        <div className="col-md-7">
                            <ul className="footer-nav text-md-left text-center m-b-sm-15">
                                <li>
                                    <a href="https://radcred.com/privacy-policy/" target="_blank">Privacy &amp; Policy</a>
                                </li>
                                <li>
                                    <a href="#">Conditions</a>
                                </li>
                                <li>
                                    <p>Powerd by  <a href="https://radcred.com/" target="_blank">radcred</a></p>
                                </li>
                            </ul>
                        </div>
                        <div className="col-md-5">
                            <div className="copyright-text text-md-right text-center">
                                <p>© 2022
                                    <a className='ml-2' href="/index"> Radcred</a>. All Rights Reserved</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}
export default Footer;