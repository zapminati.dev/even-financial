import React from "react";
import {Button, OverlayTrigger, Popover} from "react-bootstrap";

const Header = () => {

     return (
          <nav
               className="navbar navbar-expand-lg navbar-light  "
               style={{
                    background: "#593e6f ",
               }}
          >
               <div className="container-fluid">
                    {/* <a className="navbar-brand" href="#">Radcred</a> */}
                    <a className="navbar-brand" href="https://radcred.com/">
                         <img
                              src="https://radcred.com/wp-content/uploads/2020/01/Group-11863-1.png"
                              width={"150px"}
                              className="main-logo"
                              alt=""
                         />
                    </a>
                    <button
                         className="navbar-toggler"
                         type="button"
                         data-bs-toggle="collapse"
                         data-bs-target="#navbarSupportedContent"
                         aria-controls="navbarSupportedContent"
                         aria-expanded="false"
                         aria-label="Toggle navigation"
                    >
                         <span className="navbar-toggler-icon"></span>
                    </button>
                    <div
                         className="collapse navbar-collapse"
                         id="navbarSupportedContent"
                    >
                         <div
                              className="collapse navbar-collapse"
                              id="navbarSupportedContent"
                         >
                              <ul className="navbar-nav ml-auto">
                                   <li className="nav-item">
                                        <OverlayTrigger
                                            trigger="click"
                                             placement="bottom"
                                             key="bottom"
                                             overlay={
                                                  <Popover
                                                       id="popover-positioned-bottom"
                                                  >
                                                       <Popover.Title as="h3">Advertisement Discloser</Popover.Title>
                                                       <Popover.Content className="text-align-justify">
                                                            He offers that appear are from companies which (RadCred) and its partners receive compensation. This compensation may influence the selection, appearance, and order of appearance of the offers listed below. However, this compensation also facilitates the provision by (RadCred) of certain services to you at no charge. The offers shown below do not include all financial services companies or all of their available product and service offerings.
                                                       </Popover.Content>
                                                  </Popover>
                                             }
                                        >
                                             <Button variant="secondary">
                                                            <i className="fa fa-lock text-success mr-2"></i>
                                                            AD Discloser
                                                       
                                                  </Button>
                                             
                                        </OverlayTrigger>
                                   </li>
                              </ul>
                         </div>
                    </div>
               </div>
          </nav>
     );
};
export default Header;
