import React, { useState } from "react";
import { Accordion, Tab } from "react-bootstrap";



const FormFooter = () => {
     const [toggle, setToggle] = useState(1);

     return (
          <>
               <section className="sign-in-section p-t-0">
                    
                         <div className="row justify-content-center">
                              <div className="col-sm-12 col-lg-8 col-xs-6 p-0">
                                   
                                        <div className="landio-accordion-v2">
                                             <Accordion
                                                  className="accordion"
                                                  id="generalFAQ"
                                                  defaultActiveKey="collapseOne"
                                             >
                                                  <div className="accordion-item">
                                                       <div className="accordion-header">
                                                            <Accordion.Toggle
                                                                 as="button"
                                                                 eventKey="collapseOne"
                                                                 className="accordion-button"
                                                                 type="button"
                                                                 onClick={() =>
                                                                      setToggle(
                                                                           toggle ===
                                                                                1
                                                                                ? 0
                                                                                : 1
                                                                      )
                                                                 }
                                                                 aria-expanded={
                                                                      toggle ===
                                                                      1
                                                                           ? "true"
                                                                           : "false"
                                                                 }
                                                            >
                                                                 General
                                                                 Disclosure
                                                            </Accordion.Toggle>
                                                       </div>
                                                       <Accordion.Collapse eventKey="collapseOne">
                                                            <div className="accordion-body">
                                                                 <p>
                                                                      All rates,
                                                                      fees, and
                                                                      terms are
                                                                      presented
                                                                      without
                                                                      guarantee
                                                                      and are
                                                                      subject to
                                                                      change
                                                                      pursuant
                                                                      to each
                                                                      provider's
                                                                      discretion
                                                                      and may
                                                                      not be
                                                                      available
                                                                      in all
                                                                      states or
                                                                      for all
                                                                      types of
                                                                      loans.
                                                                      There is
                                                                      no
                                                                      guarantee
                                                                      you will
                                                                      be
                                                                      approved
                                                                      or qualify
                                                                      for the
                                                                      advertised
                                                                      rates,
                                                                      fees, or
                                                                      terms
                                                                      presented.
                                                                      This
                                                                      website
                                                                      does not
                                                                      include
                                                                      all
                                                                      lending
                                                                      companies
                                                                      or all
                                                                      available
                                                                      lending
                                                                      offers
                                                                      that may
                                                                      be
                                                                      available
                                                                      to you.
                                                                      You should
                                                                      read the
                                                                      lender's
                                                                      personal
                                                                      terms and
                                                                      conditions,
                                                                      as well as
                                                                      any
                                                                      disclosures,
                                                                      in full
                                                                      before
                                                                      proceeding
                                                                      for any
                                                                      loan.
                                                                 </p>
                                                            </div>
                                                       </Accordion.Collapse>
                                                  </div>
                                                  <div className="accordion-item">
                                                       <h5
                                                            className="accordion-header"
                                                            id="headingTwo"
                                                       >
                                                            <Accordion.Toggle
                                                                 as="button"
                                                                 eventKey="collapse2"
                                                                 className="accordion-button"
                                                                 type="button"
                                                                 onClick={() =>
                                                                      setToggle(
                                                                           toggle ===
                                                                                2
                                                                                ? 0
                                                                                : 2
                                                                      )
                                                                 }
                                                                 aria-expanded={
                                                                      toggle ===
                                                                      2
                                                                           ? "true"
                                                                           : "false"
                                                                 }
                                                            >
                                                                 APR Disclosure
                                                            </Accordion.Toggle>
                                                       </h5>
                                                       <Accordion.Collapse eventKey="collapse2">
                                                            <div className="accordion-body">
                                                                 <p>
                                                                      The Annual
                                                                      Percentage
                                                                      Rate is
                                                                      the rate
                                                                      at which
                                                                      your loan
                                                                      accrues
                                                                      interest.
                                                                      It is
                                                                      based upon
                                                                      the amount
                                                                      of your
                                                                      loan, cost
                                                                      of the
                                                                      loan, term
                                                                      of the
                                                                      loan,
                                                                      repayment
                                                                      amounts
                                                                      and timing
                                                                      of
                                                                      payments
                                                                      and
                                                                      payoff. By
                                                                      law, the
                                                                      lender or
                                                                      lending
                                                                      partner/platform
                                                                      must show
                                                                      you the
                                                                      APR before
                                                                      you enter
                                                                      into the
                                                                      loan.
                                                                      States
                                                                      have laws
                                                                      that may
                                                                      limit the
                                                                      APR that
                                                                      the lender
                                                                      or lending
                                                                      partner/platform
                                                                      can charge
                                                                      you. Rates
                                                                      will vary
                                                                      based on
                                                                      your
                                                                      credit
                                                                      worthiness,
                                                                      loan size,
                                                                      and other
                                                                      variables,
                                                                      with the
                                                                      lowest
                                                                      rates
                                                                      available
                                                                      to
                                                                      customers
                                                                      with
                                                                      excellent
                                                                      credit.
                                                                      Minimum
                                                                      and
                                                                      maximum
                                                                      loan
                                                                      amounts
                                                                      and APRs
                                                                      may vary
                                                                      according
                                                                      to state
                                                                      law and
                                                                      lender or
                                                                      lending
                                                                      partner/platform.
                                                                      We
                                                                      recommend
                                                                      that you
                                                                      read the
                                                                      lender’s
                                                                      and/or
                                                                      lending
                                                                      partner’s/platform’s
                                                                      personal
                                                                      terms and
                                                                      conditions
                                                                      in full
                                                                      before
                                                                      proceeding
                                                                      for
                                                                      apersonal
                                                                      loan.
                                                                 </p>
                                                            </div>
                                                       </Accordion.Collapse>
                                                  </div>
                                                  <div className="accordion-item">
                                                       <h5
                                                            className="accordion-header"
                                                            id="headingThree"
                                                       >
                                                            <Accordion.Toggle
                                                                 as="button"
                                                                 eventKey="collapse3"
                                                                 className="accordion-button"
                                                                 type="button"
                                                                 onClick={() =>
                                                                      setToggle(
                                                                           toggle ===
                                                                                3
                                                                                ? 0
                                                                                : 3
                                                                      )
                                                                 }
                                                                 aria-expanded={
                                                                      toggle ===
                                                                      3
                                                                           ? "true"
                                                                           : "false"
                                                                 }
                                                            >
                                                                 Representative
                                                                 Example
                                                            </Accordion.Toggle>
                                                       </h5>
                                                       <Accordion.Collapse eventKey="collapse3">
                                                            <div className="accordion-body">
                                                                 <p>
                                                                      If you
                                                                      borrow
                                                                      $5,000 on
                                                                      a 3 year
                                                                      repayment
                                                                      term and
                                                                      at a 10%
                                                                      APR, the
                                                                      monthly
                                                                      repayment
                                                                      will be
                                                                      $161.34.
                                                                      Total
                                                                      repayment
                                                                      will be
                                                                      $5,808.24.
                                                                      Total
                                                                      interest
                                                                      paid will
                                                                      be
                                                                      $808.24.
                                                                 </p>
                                                            </div>
                                                       </Accordion.Collapse>
                                                  </div>
                                             </Accordion>
                                             
                                   </div>
                              </div>
                         </div>
                    
               </section>
          </>
     );
};
export default FormFooter;
