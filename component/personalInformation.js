import React, { StrictMode, useState } from "react";
import Link from "next/link";
import validator from "validator";

import Header from "./layouts/header";
import Footer from "./layouts/footer";
import { isValid } from "ssn-validator/dist/src";
import InputMask from 'react-input-mask'

const StepOne = ({ nextStep, handleFormData, prevStep, values }) => {
     const [error, setError] = useState({
          fname: false,
          lname: false,
          mobile: false,
          email: false,
          add: false,
          city: false,
          state: false,
          zip: false,
          dob: false,
          ssn: false,

     });
     
     const [errorMessage, setErrorMessage] = useState("");
     // const [phoneMask, setPhoneMask] = useState(values.mobile);

     // const Phone = props =>{
     //      <InputMask mask="999-99-999" value={values.mobile} onChange={handleFormData('mobile')} />
     // }

     // after form submit validating the form data using validator
     const submitFormData = (e) => {
          e.preventDefault();

          // checking if value of first name and last name is empty show error else take to step 2
          // if (
          //      validator.isEmpty(values.firstName) ||
          //      (values.firstName.length > 60) ||
          //      validator.isEmpty(values.lastName) ||
          //      (values.lastName.length > 60) ||
          //      validator.isEmpty(values.email) ||
          //      (values.email.length > 60) ||
          //      validator.isEmpty(values.address) ||
          //      (values.address.length > 80) ||
          //      // validator.isEmpty(values.mobile) ||
          //      // !validator.isMobilePhone(values.mobile, "en-US") ||
          //      validator.isEmpty(values.city) ||
          //      validator.isEmpty(values.state) ||
          //      validator.isEmpty(values.zip) ||
          //      !validator.isPostalCode(values.zip, "US")||
          //      // (values.zip.length !== 5) ||
          //      validator.isEmpty(toString(values.dateOfBirth)) ||
          //      !isValid(values.socialSecurityNumber) ||
          //      !validator.isDate(values.dateOfBirth, "mm-dd-YYYY")
          // ) {
          //      setError(true);
          // } else {
          //      setError(false);
          //      nextStep();
          // }

          if(values.firstName.length > 60){ 
                    setErrorMessage("First name shouldn't more than 60 char.")
                    setError({fname: true})
                    return false;
          }else if (values.lastName.length > 60) {
               setErrorMessage("Last name shouldn't more than 60 char.")
               setError({lname: true})
               return false;
          }else if (validator.isEmpty(values.mobile) ||
          !validator.isMobilePhone(values.mobile, "en-US")) {
               setErrorMessage("Please enter vailid mobile number.")
               setError({mobile: true})
               return false;
          }else if (values.email.length > 60 || !validator.isEmail(values.email)) {
               setErrorMessage("Please enter vailid email Id.")
               setError({email: true})
               return false;
          }else if (values.address.length > 80) {
               setErrorMessage("Address shouldn't more than 80 char.")
               setError({add: true})
               return false;
          }else if (!validator.isPostalCode(values.zip, "US")) {
               setErrorMessage("Please enter vailid zip code.")
               setError({zip: true})
               return false;
          }else if (!isValid(values.socialSecurityNumber)) {
               setErrorMessage("Please enter vailid social security number.")
               setError({ssn: true})
               return false;    
          }else if (!validator.isDate(values.dateOfBirth, "mm-dd-YYYY")) {
               setErrorMessage("DOB should be in MM-DD-YYYY")
               setError({dob: true})
               return false;
          }
          else{
               setErrorMessage("")
               setError(err=> err === true ? false : false)
               nextStep();
          }
     };


     return (
          <>
               <section className="sign-in-section p-t-50 p-b-0">
                    <div className="container">
                         <div className="row justify-content-center">
                              <div className="col-xl-6 col-lg-8 col-md-12 col-sm-12">
                                   <div className="mb-3">
                                        <div className="progress">
                                             <div
                                                  className="progress-bar"
                                                  role="progressbar"
                                                  style={{ width: "14%" }}
                                                  aria-valuenow="14"
                                                  aria-valuemin="0"
                                                  aria-valuemax="100"
                                             ></div>
                                        </div>
                                   </div>
                                   <div className="sign-in-up-wrapper">
                                        <form onSubmit={submitFormData}>
                                             <div className="form-groups">
                                                  <h4 className="form-title">
                                                       Personal Information
                                                  </h4>
                                                  {/* {error ? ( */}
                                                       <span className="mb-2 text-10 text-danger">
                                                            {
                                                                 errorMessage
                                                            }
                                                       </span>
                                                  {/* ) : (
                                                       ""
                                                  )} */}
                                                  {/*Name*/}
                                                  <div className="row g-3 ">
                                                       <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 mb-3">
                                                            <div className="field-group">
                                                                 <div className="icon">
                                                                      <i className="far fa-user"></i>
                                                                 </div>
                                                                 <input
                                                                      name="firstName"
                                                                      defaultValue={
                                                                           values.firstName
                                                                      }
                                                                      type="text"
                                                                      placeholder="First Name"
                                                                      onChange={handleFormData("firstName")}
                                                                      className={
                                                                           error.fname
                                                                                ? "border border-danger"
                                                                                : ""
                                                                      }
                                                                      required
                                                                 />
                                                            </div>
                                                       </div>
                                                       <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 mb-3">
                                                            <div className="field-group">
                                                                 <div className="icon">
                                                                      <i className="far fa-user"></i>
                                                                 </div>
                                                                 <input
                                                                      name="lastName"
                                                                      defaultValue={
                                                                           values.lastName
                                                                      }
                                                                      type="text"
                                                                      placeholder="Last Name"
                                                                      onChange={handleFormData(
                                                                           "lastName"
                                                                      )}
                                                                      className={
                                                                           error.lname
                                                                                ? "border border-danger"
                                                                                : ""
                                                                      }
                                                                      required
                                                                 />
                                                            </div>
                                                       </div>
                                                  </div>
                                                  {/*address*/}
                                                  <div className="row g-3 ">
                                                       <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 mb-3">
                                                            <div className="field-group">
                                                                 <div className="icon">
                                                                      <i className="fa fa-address-card"></i>
                                                                 </div>
                                                                 <input
                                                                      name="address"
                                                                      defaultValue={
                                                                           values.address
                                                                      }
                                                                      type="text"
                                                                      placeholder="Address"
                                                                      onChange={handleFormData(
                                                                           "address"
                                                                      )}
                                                                      className={
                                                                           error.add
                                                                                ? "border border-danger"
                                                                                : ""
                                                                      }
                                                                      required
                                                                 />
                                                            </div>
                                                       </div>

                                                       <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 mb-3">
                                                            <div className="field-group">
                                                                 <div className="icon">
                                                                      <i className="fa fa-address-card"></i>
                                                                 </div>
                                                                 <input
                                                                      name="address2"
                                                                      defaultValue={
                                                                           values.address2
                                                                      }
                                                                      type="text"
                                                                      placeholder="Address Line 2"
                                                                      onChange={handleFormData(
                                                                           "address2"
                                                                      )}
                                                                      // className={
                                                                      //      error
                                                                      //           ? "border border-danger"
                                                                      //           : ""
                                                                      // }
                                                                 />
                                                            </div>
                                                       </div>
                                                  </div>
                                                  <div className="row g-3 ">
                                                       <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 mb-3">
                                                            <div className="field-group">
                                                                 <div className="icon">
                                                                      <i className="far fa-phone"></i>
                                                                 </div>

                                                                 {/* <input
                                                                      type="tel"
                                                                      placeholder="Mobile"
                                                                      name="mobile"
                                                                      defaultValue={
                                                                           values.mobile
                                                                      }
                                                                      onChange={handleFormData(
                                                                           "mobile"
                                                                      )}
                                                                      className={
                                                                           error
                                                                                ? "border border-danger"
                                                                                : ""
                                                                      }
                                                                 /> */}
                                                                 <InputMask 
                                                                      mask="999-999-9999" 
                                                                      defaultValue={values.mobile} 
                                                                      onChange={handleFormData('mobile')} 
                                                                      className={
                                                                           error.mobile
                                                                                ? "border border-danger"
                                                                                : ""
                                                                      }
                                                                      placeholder="Mobile"
                                                                      required
                                                                 />

                                                                 <div className="icon tooltip_icon">
                                                                      <i
                                                                           className="fa fa-info-circle"
                                                                           data-toggle="tooltip"
                                                                           data-placement="bottom"
                                                                           title="We ask this so that we, our partners and their affiliated companies can contact 
                            you about the products and services you inquired about, even if your telephone number is listed on any Do-Not-Call list. Contact may be made through automatic dialing systems, 
                            artificial or prerecorded voice messaging, or text message."
                                                                      ></i>
                                                                 </div>
                                                            </div>
                                                       </div>

                                                       <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 mb-3">
                                                            <div className="field-group">
                                                                 <div className="icon">
                                                                      <i className="far fa-envelope"></i>
                                                                 </div>
                                                                 <input
                                                                      type="email"
                                                                      placeholder="Email Address"
                                                                      name="email"
                                                                      defaultValue={
                                                                           values.email
                                                                      }
                                                                      onChange={handleFormData(
                                                                           "email"
                                                                      )}
                                                                      className={
                                                                           error.email
                                                                                ? "border border-danger"
                                                                                : ""
                                                                      }
                                                                      required
                                                                 />
                                                            </div>
                                                       </div>
                                                  </div>
                                                  {/* city*/}
                                                  <div className="row g-3 ">
                                                       <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 mb-3">
                                                            <div className="field-group">
                                                                 <div className="icon">
                                                                      <i className="far fa-city"></i>
                                                                 </div>
                                                                 <input
                                                                      name="city"
                                                                      defaultValue={
                                                                           values.city
                                                                      }
                                                                      type="text"
                                                                      placeholder="City"
                                                                      onChange={handleFormData(
                                                                           "city"
                                                                      )}
                                                                      className={
                                                                           error.city
                                                                                ? "border border-danger"
                                                                                : ""
                                                                      }
                                                                      required
                                                                 />
                                                            </div>
                                                       </div>

                                                       <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 mb-3">
                                                            <div className="field-group">
                                                                 <div className="icon">
                                                                      <i className="far fa-city"></i>
                                                                 </div>

                                                                 <select
                                                                      name="state"
                                                                      defaultValue={
                                                                           values.state
                                                                      }
                                                                      type="text"
                                                                      placeholder="State"
                                                                      onChange={handleFormData(
                                                                           "state"
                                                                      )}
                                                                      className={
                                                                           error.state
                                                                                ? "border border-danger"
                                                                                : ""
                                                                      }
                                                                      required
                                                                 >
                                                                      <option value="">
                                                                           Select
                                                                           State
                                                                      </option>
                                                                      <option value="AL">
                                                                           Alabama
                                                                      </option>
                                                                      <option value="AK">
                                                                           Alaska
                                                                      </option>
                                                                      <option value="AZ">
                                                                           Arizona
                                                                      </option>
                                                                      <option value="AR">
                                                                           Arkansas
                                                                      </option>
                                                                      <option value="CA">
                                                                           California
                                                                      </option>
                                                                      <option value="CO">
                                                                           Colorado
                                                                      </option>
                                                                      <option value="CT">
                                                                           Connecticut
                                                                      </option>
                                                                      <option value="DE">
                                                                           Delaware
                                                                      </option>
                                                                      <option value="DC">
                                                                           District
                                                                           of
                                                                           Columbia
                                                                      </option>
                                                                      <option value="FL">
                                                                           Florida
                                                                      </option>
                                                                      <option value="GA">
                                                                           Georgia
                                                                      </option>
                                                                      <option value="HI">
                                                                           Hawaii
                                                                      </option>
                                                                      <option value="ID">
                                                                           Idaho
                                                                      </option>
                                                                      <option value="IL">
                                                                           Illinois
                                                                      </option>
                                                                      <option value="IN">
                                                                           Indiana
                                                                      </option>
                                                                      <option value="IA">
                                                                           Iowa
                                                                      </option>
                                                                      <option value="KS">
                                                                           Kansas
                                                                      </option>
                                                                      <option value="KY">
                                                                           Kentucky
                                                                      </option>
                                                                      <option value="LA">
                                                                           Louisiana
                                                                      </option>
                                                                      <option value="ME">
                                                                           Maine
                                                                      </option>
                                                                      <option value="MD">
                                                                           Maryland
                                                                      </option>
                                                                      <option value="MA">
                                                                           Massachusetts
                                                                      </option>
                                                                      <option value="MI">
                                                                           Michigan
                                                                      </option>
                                                                      <option value="MN">
                                                                           Minnesota
                                                                      </option>
                                                                      <option value="MS">
                                                                           Mississippi
                                                                      </option>
                                                                      <option value="MO">
                                                                           Missouri
                                                                      </option>
                                                                      <option value="MT">
                                                                           Montana
                                                                      </option>
                                                                      <option value="NE">
                                                                           Nebraska
                                                                      </option>
                                                                      <option value="NV">
                                                                           Nevada
                                                                      </option>
                                                                      <option value="NH">
                                                                           New
                                                                           Hampshire
                                                                      </option>
                                                                      <option value="NJ">
                                                                           New
                                                                           Jersey
                                                                      </option>
                                                                      <option value="NM">
                                                                           New
                                                                           Mexico
                                                                      </option>
                                                                      <option value="NY">
                                                                           New
                                                                           York
                                                                      </option>
                                                                      <option value="NC">
                                                                           North
                                                                           Carolina
                                                                      </option>
                                                                      <option value="ND">
                                                                           North
                                                                           Dakota
                                                                      </option>
                                                                      <option value="OH">
                                                                           Ohio
                                                                      </option>
                                                                      <option value="OK">
                                                                           Oklahoma
                                                                      </option>
                                                                      <option value="OR">
                                                                           Oregon
                                                                      </option>
                                                                      <option value="PA">
                                                                           Pennsylvania
                                                                      </option>
                                                                      <option value="RI">
                                                                           Rhode
                                                                           Island
                                                                      </option>
                                                                      <option value="SC">
                                                                           South
                                                                           Carolina
                                                                      </option>
                                                                      <option value="SD">
                                                                           South
                                                                           Dakota
                                                                      </option>
                                                                      <option value="TN">
                                                                           Tennessee
                                                                      </option>
                                                                      <option value="TX">
                                                                           Texas
                                                                      </option>
                                                                      <option value="UT">
                                                                           Utah
                                                                      </option>
                                                                      <option value="VT">
                                                                           Vermont
                                                                      </option>
                                                                      <option value="VA">
                                                                           Virginia
                                                                      </option>
                                                                      <option value="WA">
                                                                           Washington
                                                                      </option>
                                                                      <option value="WV">
                                                                           West
                                                                           Virginia
                                                                      </option>
                                                                      <option value="WI">
                                                                           Wisconsin
                                                                      </option>
                                                                      <option value="WY">
                                                                           Wyoming
                                                                      </option>
                                                                 </select>
                                                            </div>
                                                       </div>
                                                  </div>
                                                  {/* city*/}
                                                  <div className="row g-3 ">
                                                       <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 mb-3">
                                                            <div className="field-group">
                                                                 <div className="icon">
                                                                      <svg
                                                                           xmlns="http://www.w3.org/2000/svg"
                                                                           width="16"
                                                                           height="16"
                                                                           fill="currentColor"
                                                                           className="bi bi-file-zip"
                                                                           viewBox="0 0 16 16"
                                                                      >
                                                                           <path d="M6.5 7.5a1 1 0 0 1 1-1h1a1 1 0 0 1 1 1v.938l.4 1.599a1 1 0 0 1-.416 1.074l-.93.62a1 1 0 0 1-1.109 0l-.93-.62a1 1 0 0 1-.415-1.074l.4-1.599V7.5zm2 0h-1v.938a1 1 0 0 1-.03.243l-.4 1.598.93.62.93-.62-.4-1.598a1 1 0 0 1-.03-.243V7.5z" />
                                                                           <path d="M2 2a2 2 0 0 1 2-2h8a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V2zm5.5-1H4a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h8a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H9v1H8v1h1v1H8v1h1v1H7.5V5h-1V4h1V3h-1V2h1V1z" />
                                                                      </svg>
                                                                 </div>
                                                                 <input
                                                                      name="zip"
                                                                      defaultValue={
                                                                           values.zip
                                                                      }
                                                                      type="number"
                                                                      placeholder="ZIP"
                                                                      onChange={handleFormData(
                                                                           "zip"
                                                                      )}
                                                                      className={
                                                                           error.zip
                                                                                ? "border border-danger"
                                                                                : ""
                                                                      }
                                                                      required
                                                                 />
                                                            </div>
                                                       </div>

                                                       <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 mb-3">
                                                            <div className="field-group">
                                                                 <div className="icon">
                                                                      <i className="far fa-calendar"></i>
                                                                 </div>
                                                                 {/* <input
                                                                      id="datepicker "
                                                                      name="dateOfBirth"
                                                                      defaultValue={
                                                                           values.dateOfBirth
                                                                      }
                                                                      type="text"
                                                                      placeholder="YYYY-MM-DD"
                                                                      required
                                                                      pattern="\d{4}-\d{2}-\d{2}"
                                                                      onChange={handleFormData(
                                                                           "dateOfBirth"
                                                                      )}
                                                                      className={
                                                                           error
                                                                                ? "border border-danger"
                                                                                : "md-form md-outline input-with-post-icon datepicker"
                                                                      }
                                                                 /> */}
                                                                 <InputMask 
                                                                      mask="99-99-9999" 
                                                                      defaultValue={values.dateOfBirth} 
                                                                      onChange={handleFormData('dateOfBirth')} 
                                                                      className={
                                                                           error.dob
                                                                                ? "border border-danger"
                                                                                : ""
                                                                      }
                                                                      placeholder="DOB (MM-DD-YYYY)"
                                                                      required
                                                                 />
                                                                 <span className="input-group-addon">
                                                                      <i className="glyphicon glyphicon-calendar"></i>
                                                                 </span>
                                                            </div>
                                                       </div>
                                                  </div>
                                                  <div className="field-group">
                                                       <div className="icon">
                                                            <i className="far fa-key"></i>
                                                       </div>
                                                       {/* <input
                                                            type="text"
                                                            placeholder="SSN (eg: 123-12-1234)"
                                                            name="socialSecurityNumber"
                                                            pattern="\d{3}-\d{2}-\d{4}"
                                                            defaultValue={
                                                                 values.socialSecurityNumber
                                                            }
                                                            onChange={handleFormData(
                                                                 "socialSecurityNumber"
                                                            )}
                                                            className={
                                                                 error
                                                                      ? "border border-danger"
                                                                      : ""
                                                            }
                                                       /> */}
                                                       <InputMask 
                                                            mask="999-99-9999" 
                                                            defaultValue={values.socialSecurityNumber} 
                                                            onChange={handleFormData('socialSecurityNumber')} 
                                                            className={
                                                                 error.ssn
                                                                      ? "border border-danger"
                                                                      : ""
                                                            }
                                                            placeholder="Social Security Number"
                                                            required
                                                       />
                                                       <div className="icon tooltip_icon">
                                                            <i
                                                                 className="fa fa-info-circle"
                                                                 data-toggle="tooltip"
                                                                 data-placement="bottom"
                                                                 title="We ask this so that we, our partners and their affiliated companies can contact 
                            you about the products and services you inquired about, even if your telephone number is listed on any Do-Not-Call list. Contact may be made through automatic dialing systems, 
                            artificial or prerecorded voice messaging, or text message."
                                                            ></i>
                                                       </div>
                                                  </div>
                                             </div>
                                             <div className="form-note">
                                                  <div className="field-group">
                                                       <button type="submit">
                                                            Next
                                                       </button>
                                                  </div>
                                                  <div className="field-group">
                                                       <Link href="#">
                                                            <a
                                                                 type="submit"
                                                                 onClick={
                                                                      prevStep
                                                                 }
                                                            >
                                                                 Back
                                                            </a>
                                                       </Link>
                                                  </div>
                                             </div>
                                        </form>
                                   </div>
                              </div>
                         </div>
                    </div>
               </section>
          </>
     );
};

export default StepOne;
