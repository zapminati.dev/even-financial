import React, { useState } from "react";
import Link from "next/link";
import validator from "validator";

const StepFive = ({ nextStep, handleFormData, prevStep, values }) => {
     const [error, setError] = useState(false);

     // after form submit validating the form data using validator
     const submitFormData = (e) => {
          e.preventDefault();

          // checking if value of first name and last name is empty show error else take to step 2
          if (
               // validator.isEmpty(toString(values.employmentStatus)) ||
               //validator.isEmpty(toString(values.payFrequency)) ||
               values.employmentStatus === undefined ||
               values.employmentStatus === ""
                    ? true
                    : "" || validator.isLength(values.employmentStatus)
                    ? ""
                    : true ||
                      values.payFrequency === undefined ||
                      values.payFrequency === ""
                    ? true
                    : "" ||
                      validator.isLength(values.payFrequency, {
                           min: 5,
                           max: 5,
                      })
                    ? ""
                    : true ||
                      //validator.isEmpty(toString(values.annualIncome))
                      values.annualIncome === undefined ||
                      values.annualIncome === ""
                    ? true
                    : "" ||
                      validator.isLength(values.annualIncome, {
                           min: 5,
                           max: 5,
                      })
                    ? ""
                    : true
          ) {
               setError(true);
          } else {
               setError(false);
               nextStep();
          }
     };

     return (
          <>
               <section className="sign-in-section p-t-120 p-b-120">
                    <div className="container">
                         <div className="row justify-content-center">
                              <div className="col-xl-5 col-lg-6 col-md-8 col-sm-10">
                                   <div className="mb-3">
                                        <div className="progress">
                                             <div
                                                  className="progress-bar"
                                                  role="progressbar"
                                                  style={{ width: "70%" }}
                                                  aria-valuenow="70"
                                                  aria-valuemin="0"
                                                  aria-valuemax="70"
                                             ></div>
                                        </div>
                                   </div>
                                   <div className="sign-in-up-wrapper">
                                        <form onSubmit={submitFormData}>
                                             <div className="form-groups">
                                                  <h4 className="form-title">
                                                       Employment Information
                                                  </h4>
                                                  <div className="field-group">
                                                       <div className="icon">
                                                            <i className="fas fa-user-alt"></i>
                                                       </div>

                                                       <select
                                                            name="employmentStatus"
                                                            defaultValue={
                                                                 values.employmentStatus
                                                            }
                                                            onChange={handleFormData(
                                                                 "employmentStatus"
                                                            )}
                                                            className={
                                                                 error
                                                                      ? "border border-danger"
                                                                      : ""
                                                            }
                                                            required
                                                       >
                                                            <option value="">
                                                                 Select
                                                                 Employment
                                                                 Status
                                                            </option>
                                                            <option value="employed">
                                                                 Employed
                                                            </option>
                                                            <option value="employed_full_time">
                                                                 Employed Full
                                                                 Time
                                                            </option>
                                                            <option value="employed_part_time">
                                                                 Employed Part
                                                                 Time
                                                            </option>
                                                            <option value="military">
                                                                 Military
                                                            </option>
                                                            <option value="not_employed">
                                                                 Not Employed
                                                            </option>
                                                            <option value="self_employed">
                                                                 Self Employed
                                                            </option>
                                                            <option value="retired">
                                                                 Retired
                                                            </option>
                                                            <option value="other">
                                                                 Other
                                                            </option>
                                                       </select>
                                                  </div>
                                                  <div className="field-group">
                                                       <div className="icon">
                                                            <i className="fas fa-comment-dollar"></i>
                                                       </div>

                                                       <select
                                                            name="payFrequency"
                                                            defaultValue={
                                                                 values.payFrequency
                                                            }
                                                            type="Number"
                                                            placeholder="Select Pay Frequency"
                                                            onChange={handleFormData(
                                                                 "payFrequency"
                                                            )}
                                                            className={
                                                                 error
                                                                      ? "border border-danger"
                                                                      : ""
                                                            }
                                                            required
                                                       >
                                                            <option value="">
                                                                 Select Pay
                                                                 Frequency
                                                            </option>
                                                            <option value="biweekly">
                                                                 Bi Weekly
                                                            </option>
                                                            <option value="weekly">
                                                                 Weekly
                                                            </option>
                                                            <option value="monthly">
                                                                 Monthly
                                                            </option>
                                                            <option value="twicemonthly">
                                                                 Twice monthly
                                                            </option>
                                                       </select>
                                                  </div>
                                                  <div className="field-group">
                                                       <div className="icon">
                                                            <i className="far fa-credit-card"></i>
                                                       </div>
                                                       <input
                                                            name="annualIncome"
                                                            defaultValue={
                                                                 values.annualIncome
                                                            }
                                                            type="Number"
                                                            placeholder="Annual Income"
                                                            onChange={handleFormData(
                                                                 "annualIncome"
                                                            )}
                                                            className={
                                                                 error
                                                                      ? "border border-danger"
                                                                      : ""
                                                            }
                                                            required
                                                       />
                                                       <div className="icon tooltip_icon">
                                                            <i
                                                                 className="fa fa-info-circle"
                                                                 data-toggle="tooltip"
                                                                 data-placement="bottom"
                                                                 title="List your total available including
                                                                           wages, retirement, investments and rental properties.
                                                                           Alimony, child support, or separate maintenance is
                                                                           optional and does not need to be included if you do not
                                                                           wish it to be considered as a basis for repaying the
                                                                           loan. Increase any non-taxable income or benefits by
                                                                           25%"
                                                            ></i>
                                                       </div>
                                                  </div>
                                             </div>
                                             <div className="form-note">
                                                  <div className="field-group">
                                                       <button type="submit">
                                                            Next
                                                       </button>
                                                  </div>
                                                  <div className="field-group">
                                                       <Link href="#">
                                                            <a
                                                                 type="submit"
                                                                 onClick={
                                                                      prevStep
                                                                 }
                                                            >
                                                                 Back
                                                            </a>
                                                       </Link>
                                                  </div>
                                             </div>
                                        </form>
                                   </div>
                              </div>
                         </div>
                    </div>
               </section>
          </>
     );
};

export default StepFive;
