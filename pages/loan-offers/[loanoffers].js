import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import PreLoader from "../../component/layouts/PreLoader";
import axios from "axios";
import useSWR from "swr";
import LoanOffersList from "../../component/LoanOffersList";
import OfferFooter from "../../component/layouts/OfferFooter";
import Script from "next/script";
import SpecialLoanOffers from "../../component/SpecialLoanOffers";
import DeclineLoanOffers from "../../component/DeclineLoanOffers";

const LoanOffers = () => {
     
     const router = useRouter();
     const { loanoffers } = router.query;

     const [loader, setLoader] = useState(true);
     const [interval, setInterval] = useState(0);

     //const token = "686f05a9-1a0b-442c-a48c-fd73ad6a748f_53679f03-70ac-4808-96a6-b387e022d916";
     const token = "11a972ae-da8a-4898-87b5-dad4e2af7744_a0bfd6ef-1983-469e-bae3-afac3ea4330c";

     const address = `https://api.evenfinancial.com/originator/rateTables/${loanoffers}`;
     
     // const address = `https://api.evenfinancial.com/originator/rateTables/5aa118f5-d9d1-41fb-905c-1ac74c613531`;
     const config = {
          headers: {
               "Content-type": "application/json",
               Authorization: `Bearer ${token}`,
          },
     };

     const fetcher = async (url) =>
          await axios.get(url, config).then((res) => res.data);

     const { data, error } = useSWR(
          () => {
               if (!router.query) {
                    throw new Error("Url not available yet");
               }
               return address;
          },
          fetcher,
          {
               refreshInterval: 5000,
               // revalidateOnFocus: false,
          }
     );

     if (data) {
          
          if(data.loanOffers.length>0){
          return (
               <>   
                    <LoanOffersList data={data} />
                    
                    <OfferFooter />
               </>
          );
          }else if(data.specialOffers.length>0){
               return(
                    <>
                         <SpecialLoanOffers data={data} />

                         <OfferFooter />
                    </>
               )
          }else{
               <DeclineLoanOffers/>
          }
     } else {
          return(
               <>
                    <PreLoader />;
               </>
          );
     }
     return(
          <>
          <head>
                              {/* <!-- Google tag (gtag.js) --> */}
                              <Script
                                   async
                                   src="https://www.googletagmanager.com/gtag/js?id=AW-11015730837"
                                   strategy="afterInteractive"
                              />
                              <Script id="google-analytics">
                                   {`
                                        window.dataLayer = window.dataLayer || [];
                                        function gtag(){dataLayer.push(arguments);}
                                        gtag('js', new Date());
                            
                                        gtag('config', 'AW-11015730837');
                                   `}
                              
                              </Script>
                              {/* Event snippet for Submit lead form conversion page */}
                              <Script
                              dangerouslySetInnerHTML={{
                                   __html: `gtag('event', 'conversion', {'send_to': 'AW-11015730837/JNEBCO7P64AYEJXt2oQp'})`,
                              }}
                              />
                    </head>
          </>
     )
};

export default LoanOffers;
