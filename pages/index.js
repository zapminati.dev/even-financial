import React, { useState } from "react";
import Head from "next/head";
import Image from "next/image";
import StepTwo from "../component/personalInformation";
import StepOne from "../component/loanInformation";
import StepThree from "../component/mortgageInformation";
import StepFour from "../component/creditInformation";
import StepFive from "../component/financialInformation";
import StepSix from "../component/educationInformation";
import StepSeven from "../component/legalInformation";
import Final from "../component/final";
import HeaderBanner from "../component/layouts/headerBanner";
import FooterBanner from "../component/layouts/footerBanner";
import FormFooter from "../component/layouts/formFooter";
import moment from "moment";

export default function Home() {
     //state for steps
     const [step, setstep] = useState(1);

     //state for form data
     const [formData, setFormData] = useState({
          firstName: "",
          lastName: "",
          mobile: "",
          city: "",
          email: "",
          address: "",
          address2: "",
          state: "",
          zip: "",
          dateOfBirth: "",
          socialSecurityNumber: "",
          loanPurpose: "",
          loanAmount: "",
          propertyStatus: "",
          providedCreditRating: "",
          employmentStatus: "",
          payFrequency: "",
          annualIncome: "",
          educationLevel: "",
     });

     // function for going to next step by increasing step state by 1
     const nextStep = () => {
          setstep(step + 1);
     };

     // function for going to previous step by decreasing step state by 1
     const prevStep = () => {
          setstep(step - 1);
     };

     // handling form input data by taking onchange value and updating our previous form data state
     const handleInputData = (input) => (e) => {
          // input value from the form
          const { value } = e.target;

          if(input === "dateOfBirth"){
               
               const custDob_Date = moment(value).format("MM-DD-YYYY")

               setFormData((prevState) => ({
                    ...prevState,
                    [input]: custDob_Date,
               }));
          }else{
               //updating for data state taking previous state and then adding new value to create new object
               setFormData((prevState) => ({
                    ...prevState,
                    [input]: value,
               }));
          }
          
     };

     // javascript switch case to show different form in each step
     switch (step) {
          // case 1 to show stepOne form and passing nextStep, prevStep, and handleInputData as handleFormData method as prop and also formData as value to the fprm
          case 1:
               return (
                    <>
                         <StepOne
                              nextStep={nextStep}
                              handleFormData={handleInputData}
                              values={formData}
                         />
                         {/* <FormFooter /> */}
                    </>
               );
          // case 2 to show stepTwo form passing nextStep, prevStep, and handleInputData as handleFormData method as prop and also formData as value to the fprm
          case 2:
               return (
                    <StepTwo
                         nextStep={nextStep}
                         prevStep={prevStep}
                         handleFormData={handleInputData}
                         values={formData}
                    />
               );
          case 3:
               return (
                    <>
                         <StepThree
                              nextStep={nextStep}
                              prevStep={prevStep}
                              handleFormData={handleInputData}
                              values={formData}
                         />
                         {/* <FormFooter /> */}
                    </>
               );
          case 4:
               return (
                    <>
                         <StepFour
                              nextStep={nextStep}
                              prevStep={prevStep}
                              handleFormData={handleInputData}
                              values={formData}
                         />
                         {/* <FormFooter /> */}
                    </>
               );
          case 5:
               return (
                    <>
                         <StepFive
                              nextStep={nextStep}
                              prevStep={prevStep}
                              handleFormData={handleInputData}
                              values={formData}
                         />
                         {/* <FormFooter /> */}
                    </>
               );
          case 6:
               return (
                    <>
                         <StepSix
                              nextStep={nextStep}
                              prevStep={prevStep}
                              handleFormData={handleInputData}
                              values={formData}
                         />
                         {/* <FormFooter /> */}
                    </>
               );
          case 7:
               return (
                    <>
                         <StepSeven
                              nextStep={nextStep}
                              prevStep={prevStep}
                              handleFormData={handleInputData}
                              values={formData}
                         />
                         {/* <FormFooter /> */}
                    </>
               );
          // Only formData is passed as prop to show the final value at form submit
          // case 8:
          //      return (
          //           <>
          //                <Final values={formData} /> <FormFooter />
          //           </>
          //      );
          // default case to show nothing
          default:
               return <div className="App"></div>;
     }
}
