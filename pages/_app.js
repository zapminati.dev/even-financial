import { Fragment, useEffect, useState } from "react";
import PreLoader from "../component/layouts/PreLoader";
import "../styles/globals.css";
import Header from "../component/layouts/header";
import Footer from "../component/layouts/footer";
import HeaderBanner from "../component/layouts/headerBanner";
import FooterBanner from "../component/layouts/footerBanner";

function MyApp({ Component, pageProps }) {
     const [loader, setLoader] = useState(true);
     useEffect(() => {
          setTimeout(() => {
               setLoader(false);
          }, 300);

          window.addEventListener("message", function (event) {
               // Need to check for safety as we are going to process only our messages
               // So Check whether event with data(which contains any object) contains our message here its "FrameHeight"
               if (event.data == "FrameHeight") {
                    //event.source contains parent page window object
                    //which we are going to use to send message back to main page here "abc.com/page"

                    //parentSourceWindow = event.source;

                    //Calculate the maximum height of the page
                    var body = document.body,
                         html = document.documentElement;
                    var height = Math.max(
                         body.scrollHeight,
                         body.offsetHeight,
                         html.clientHeight,
                         html.scrollHeight,
                         html.offsetHeight
                    );

                    // Send height back to parent page "abc.com/page"
                    event.source.postMessage({ FrameHeight: height }, "*");
               }
          });
     }, []);
     return (
          <Fragment>
               {loader && <PreLoader />}
               {/* <Header /> */}
               {/* <HeaderBanner/> */}

               <Component {...pageProps} />

               {/* <FooterBanner/> */}
               {/* <Footer /> */}
          </Fragment>
     );
}

export default MyApp;
