import "../../../utils/db";
const LoanRequest = require("../../../models/loanRequestModel");


export default async function handler(req, res) {
    
    if(req.method === "POST"){
        const loanRequest = await LoanRequest.create(JSON.parse(req.body));

        res.status(200).json({success: true, loanRequest});
    }

}