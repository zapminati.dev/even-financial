import mongoose from "mongoose";
import validator from "validator";

const loanRequestSchema = new mongoose.Schema({
    uuid:{
        type: String,
        required: [true, "UUID not Mentioned"]
    },
    leadUuid: {
        type: String,
        required: [true, "Lead UUID not Mentioned"]
    }
}, {timestamps: true});

module.exports = mongoose.models.LoanRequest || mongoose.model("LoanRequest", loanRequestSchema, "LoanRequest");