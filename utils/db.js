import mongoose from "mongoose";

const DB = process.env.MONGODB_URI;

mongoose.connect(DB, {
    useNewUrlParser: true,
    useUnifiedTopology: true
}).then(()=>{
    console.log(`Connection successfully!`);
}).catch((err)=>{
    console.log(`No connection! ${err}`);
})